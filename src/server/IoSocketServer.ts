import socketio from "socket.io";
import { IServer, 	ServerConfiguration, ServerCallback } from "./IServer";
import { IoSocket } from "./../socket/IoSocket";

export class IoSocketServer implements IServer
{	
	constructor(readonly config:ServerConfiguration, callback:ServerCallback)
	{
		new socketio.Server(config.port, config).on("connection", (socket:socketio.Socket) => {
			callback(new IoSocket(socket));
		});
		console.log(`Io Socket server is listening on port ${config.port}.`);
	}
};