import { ISocket } from "../socket/ISocket";

export type ServerCallback = (socket:ISocket) => void;
export type ServerConfiguration = any;

export interface IServer
{
	readonly config:ServerConfiguration;
};