import uniqid from "uniqid";
import { APlayer } from "./APlayer";
import { IReceivedPacket, ISendPacket } from "./Packet";
import { PlayerId } from "./types/PlayerId";
import { PlayerList } from "./PlayerList";
import { SocketEvent } from "./types/SocketTypes";

export type ReceiveCallback<T extends IReceivedPacket, P extends APlayer> = ((data:T) => void) | ((data:T, sender:P) => void);

export abstract class ARealtimeGame<P extends APlayer>
{
	private readonly players = new Map<PlayerId, P>();
	private readonly events = new Map<SocketEvent, ReceiveCallback<IReceivedPacket, P>>();
	private readonly registeredEvents = new Map<SocketEvent, ReceiveCallback<IReceivedPacket, P>>();
	private roomCapacity = 4;
	private hasStarted = false;
	public readonly isPublic:boolean;
	public readonly ID = uniqid();

	constructor(opts:any|number, private readonly allowMultipleConnectionsFromSameIp = false)
	{
		const isPublicRuleDefined = opts.isPublic !== undefined || opts.public !== undefined;
		const roomCapacity = typeof opts === "number" ? opts : parseInt(opts.roomCapacity);

		this.isPublic = isPublicRuleDefined ? (opts.isPublic || opts.public) : true;
		this.roomCapacity = !roomCapacity ? 2 : roomCapacity;
	}

	private prerun():void
	{
		this.registeredEvents.forEach((callback:ReceiveCallback<IReceivedPacket, P>, key:SocketEvent) => {
			this.on(key, callback);
		});
		this.run();
	}
	/**
	 * Assigns data of a disconnected player to a new user.
	 * @params player:P
	 * @returns True if user could be reconnected from an existing session, false otherwise.
	 */
	private reconnectPlayer(player:P):boolean
	{
		if (this.allowMultipleConnectionsFromSameIp)
			return (false);
		for (const p of this.players) {
			if (p[1].socket?.IP === player.socket?.IP) {
				p[1].socket = player.socket;
				return (true);
			}
		}
		return (false);
	}

	protected readonly getPlayer = (id:PlayerId):P|undefined =>
	{
		return (this.players.get(id));
	}
	/**
	 * This method will prepare a socket event to be registered.
	 * Just before the game start, all the receive events will be automatically binded.
	 * If you call this method after the game started, the event will instantly be binded.
	 *
	 * @params event:SocketEvent
	 * @params callback:(p:<T extends IReceivedPacket>)
	 */
	protected readonly registerReceiveEvent = <T extends IReceivedPacket>(event:SocketEvent, callback:ReceiveCallback<T, P>) =>
	{
		this.registeredEvents.set(event, <ReceiveCallback<IReceivedPacket, P>>callback);
		if (this.hasStarted) {
			this.on(event, callback);
		}
	}
	protected on<T extends IReceivedPacket>(event:SocketEvent, callback:ReceiveCallback<T, P>)
	{
		this.events.set(event, <ReceiveCallback<IReceivedPacket, P>>callback);
		this.apply((player:P) => {
			player.on(event, (data:T) => {
				callback(data, player);
			});
		});
	}

	protected readonly start = () =>
	{
		this.hasStarted = true;
		this.prerun();
	}
	protected readonly stop = () =>
	{
		this.apply((player:P) => {
			this.events.forEach((c, key) => {c; player.off(key)});
			this.registeredEvents.forEach((c, key) => {c; player.off(key)});
		});
		this.close();
	}
	/**
	 * Run an iterator through all the players ans execute a function on each of them.
	 * It is possible to access to the current player variable via the callback passed as parameter.
	 *
	 * @params callback:(player:<P extends APlayer>) => void = The callback to execute for every player.
	 * @returns A new instance of a PlayerList<P extends APlayer> object.
	 */
	protected apply(callback:(player:P) => void):PlayerList<P>
	{
		return (new PlayerList(this.players).apply(callback));
	}
	/**
	 * Remove each player's instance of the list that does not match the condition in your callback.
	 *
	 * @params callback:(player:<P extends APlayer>) => void = The callback to filter players.
	 * @returns The PlayerList<P extends APlayer>'s current instance.
	 */
	protected filter(callback:(player:P) => boolean):PlayerList<P>
	{
		return (new PlayerList(this.players).filter(callback));
	}
	protected onFilled()
	{
		this.start();
	}

	/**
	 * This method is called when a room is filled, so a game can start.
	 * Override this method to implement your game-logic.
	 */
	protected abstract run():void;

	protected abstract close():void;

	/**
	 * This method is automatically called when a player join the room.
	 * You can override this method to implement your own behavior.
	 *
	 * @params player:<P extends APlayer> = An object representing the player who joined the room.
	 */
	protected abstract onJoin(player:P):void;

	/**
	 * This method is automatically called when a player leave the room.
	 * You can override this method to implement your own behavior.
	 *
	 * @params player:<P extends APlayer> = An object representing the player who joined the room.
	 */
	protected abstract onDisconnect(player:P):void;

	/**
	 * This method is automatically called when a player reconnect to the room.
	 * This method may be useful to resend data that has been lost during deconnection.
	 * You can override this method to implement your own behavior.
	 *
	 * @params player:<P extends APlayer> = An object representing the player who joined the room.
	 */
	protected abstract onReconnect(player:P):void;

	/**
	 * Send a messages to all users of a room.
	 *
	 * @params event:SocketEvent = The event on which all clients will listen to.
	 * @params data:<T extends ISendPacket> = Data to send to all users.
	 */
	public broadcast<T extends ISendPacket>(event:SocketEvent, data:T)
	{
		this.apply((player:P) => {
			player.send(event, data);
		});
	}
	/**
	 * Add a player to the room. The player will be rejected if the room is filled.
	 * If player is already inside the room, its old socket is destroyed and replaced by the new.
	 * Returns true if the player was successfully added, false otherwise.
	 *
	 * @params player:<P extends APlayer>
	 * @returns A boolean indicates whether or not the player has been successfully added.
	 */
	public readonly addPlayer = (player:P):boolean =>
	{
		const isReconnecting = this.reconnectPlayer(player);

		if (this.nbPlayers >= this.roomCapacity && !player.isSpectator) {
			return (false);
		}
		this.events.forEach((callback:ReceiveCallback<IReceivedPacket, P>, key:SocketEvent) => {
			player.on(key, (data:IReceivedPacket) => callback(data, player));
		});
		if (isReconnecting) {
			this.onReconnect(player);
			return (true);
		}
		this.players.set(player.ID, player);
		this.onJoin(player);
		if (!this.hasStarted && this.isFilled) {
			this.onFilled();
		}
		return (true);
	}
	/**
	 * Remove a player from the room, regardless of whether the socket is disconnected or not.
	 * Returns true if the player was successfully removed, false otherwise.
	 *
	 * @params player:<P extends APlayer>
	 * @returns A boolean indicates whether or not the player has been successfully removed.
	 */
	public readonly removePlayer = (player:P):boolean =>
	{
		const p = this.getPlayer(player.ID);

		if (!p)
			return (false);
		this.players.delete(p.ID);
		this.onDisconnect(player);
		return (true);
	}

	get nbPlayers()
	{
		return (this.filter((p) => !p.isSpectator).length);
	}
	get isFilled()
	{
		return (this.nbPlayers === this.roomCapacity);
	}
};