import { IReceivedPacket, ISendPacket } from "./Packet";

export interface RoomIdentityPacket extends IReceivedPacket
{
	room?:string;
	name?:string;
	roomName?:string;
	roomId?:string;
	opts:any;
	playerOpts?:any;
}

export interface ResponsePacket extends ISendPacket
{
    status:string;
    message:string;
	id?:string;
}