import { APlayer } from "./APlayer";
import { ARealtimeGame } from "./ARealtimeGame";
import { RoomInstanciator } from "./RoomInstanciator";
import { IServer, ServerConfiguration } from "./server/IServer";
import { IoSocketServer } from "./server/IoSocketServer";
import { TcpSocketServer } from "./server/TcpSocketServer";
import { WebSocketServer } from "./server/WebSocketServer";
import { ISocket } from "./socket/ISocket";
import { RoomIdentityPacket } from "./SystemPackets";

type ServerData = {type:SocketServer.Type, config:ServerConfiguration, server?:IServer};

export class SocketServer<R extends ARealtimeGame<P>, P extends APlayer>
{
	private static readonly MIN_BINDED_PORT = 1024;
	private static readonly MAX_BINDED_PORT = 65535;
	private static readonly DEFAULT_ROOMPATH_NAME = "default";
	private readonly servers:ServerData[] = [];
	protected readonly roomInstanciators = new Map<string, RoomInstanciator<R, P>>();
	public enablePrivateRooms = false;

	constructor(RInstanciator?:(new (opts?:any) => R), PInstanciator?:(new (opts?:any) => P))
	{
		let ri:RoomInstanciator<R, P>;

		if (RInstanciator && PInstanciator) {
			ri = new RoomInstanciator(RInstanciator, PInstanciator);
			this.roomInstanciators.set(SocketServer.DEFAULT_ROOMPATH_NAME, ri);
		}
	}

	private setupPlayer = (socket:ISocket) =>
	{
		const defaultInstanciator = this.roomInstanciators.get(SocketServer.DEFAULT_ROOMPATH_NAME);

		if (defaultInstanciator) {
			defaultInstanciator.instanciatePlayerToRoom(socket, {});
			return;
		}
		socket.on("room", (packet:RoomIdentityPacket) => {
			const room = packet.room || packet.name || packet.roomName || packet.roomId;
			const instanciator = this.roomInstanciators.get(room!);

			if (!instanciator)
				return;
			socket.remove("room");
			instanciator.instanciatePlayerToRoom(socket, packet.opts, packet.playerOpts);
		});
	}
	
	/**
	 * Create a new subtype of room with its own rules, inherited from the server instanciators types.
	 * To access this subroom, client must send an event named "room".
	 * This event must also send a variable named "room".
	 * It must contains the name you pass as parameter of this function to be allowed to join the room.
	 *
	 * @params name:string = The name of the subroom type that the client can access.
	 * @params RInstanciator:(new (opts?:any) => R) = The subroom type (inherited from the default templated class R)
	 * @params RInstanciator:(new (opts?:any) => P) = The subplayer type (inherited from the default templated class P)
	 */
	public addInstanciator(name:string, RInstanciator:(new (opts?:any) => R), PInstanciator:(new (opts?:any) => P))
	{
		this.roomInstanciators.set(name, new RoomInstanciator<R, P>(RInstanciator, PInstanciator));
	}
	
	/**
	 *
	 */
	public createServer(serverType:SocketServer.Type, config:ServerConfiguration)
	{
		if (!config?.port || config.port < SocketServer.MIN_BINDED_PORT || config.port > SocketServer.MAX_BINDED_PORT)
			return;
		for (let i = this.servers.length - 1; i >= 0; i--) {
			if (this.servers[i].config.port === config.port)
				return;
		}
		this.servers.push(<ServerData>{type: serverType, config});
	}

	/**
	 * Creates the servers whose a port has been binded to, and set them ready to listen to sockets on this port.
	 */
	public run():boolean
	{
		if (this.roomInstanciators.size == 0)
			return (false);
		this.servers.forEach((s:ServerData) => {
			if (s.type === SocketServer.Type.IO)
				s.server = new IoSocketServer(s.config, this.setupPlayer);
			if (s.type === SocketServer.Type.TCP)
				s.server = new TcpSocketServer(s.config, this.setupPlayer);
			if (s.type === SocketServer.Type.WS)
				s.server = new WebSocketServer(s.config, this.setupPlayer);
		});
		return (true);
	}
};

export module SocketServer
{
	export enum Type
	{
		IO,
		TCP,
		WS
	};
};